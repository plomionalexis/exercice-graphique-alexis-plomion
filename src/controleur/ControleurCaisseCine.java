
package controleur;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ControleurCaisseCine {
    
    int nbEnfant = 0;
    //<editor-fold defaultstate="collapsed" desc="code généré">
    public static final String PROP_NBENFANT = "nbEnfant";
    
    public int getNbEnfant() {
        return nbEnfant;
    }
    
    public void setNbEnfant(int nbEnfant) {
        int oldNbEnfant = this.nbEnfant;
        this.nbEnfant = nbEnfant;
        propertyChangeSupport.firePropertyChange(PROP_NBENFANT, oldNbEnfant, nbEnfant);
    }
    //</editor-fold>
    int nbEtudiant = 0;
    //<editor-fold defaultstate="collapsed" desc="code généré">
    public static final String PROP_NBETUDIANT = "nbEtudiant";
    
    public int getNbEtudiant() {
        return nbEtudiant;
    }
    
    public void setNbEtudiant(int nbEtudiant) {
        int oldNbEtudiant = this.nbEtudiant;
        this.nbEtudiant = nbEtudiant;
        propertyChangeSupport.firePropertyChange(PROP_NBETUDIANT, oldNbEtudiant, nbEtudiant);
    }
    //</editor-fold>
    int nbAdultes = 0;
    //<editor-fold defaultstate="collapsed" desc="code généré">
    public static final String PROP_NBADULTES = "nbAdultes";
    
    public int getNbAdultes() {
        return nbAdultes;
    }
    
    public void setNbAdultes(int nbAdultes) {
        int oldNbAdultes = this.nbAdultes;
        this.nbAdultes = nbAdultes;
        propertyChangeSupport.firePropertyChange(PROP_NBADULTES, oldNbAdultes, nbAdultes);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
    
    boolean promotion = false;
    //<editor-fold defaultstate="collapsed" desc="code généré">
    public static final String PROP_PROMOTION = "promotion";
    
    public boolean isPromotion() {
        return promotion;
    }
    
    public void setPromotion(boolean promotion) {
        boolean oldPromotion = this.promotion;
        this.promotion = promotion;
        propertyChangeSupport.firePropertyChange(PROP_PROMOTION, oldPromotion, promotion);
    }
    //</editor-fold>

    float prixMoyen = 0;
    //<editor-fold defaultstate="collapsed" desc="code généré">
     public static final String PROP_PRIXMOYEN = "prixMoyen";
     
     public float getPrixMoyen() {
         return prixMoyen;
     }
     
     public void setPrixMoyen(float prixMoyen) {
         float oldPrixMoyen = this.prixMoyen;
         this.prixMoyen = prixMoyen;
         propertyChangeSupport.firePropertyChange(PROP_PRIXMOYEN, oldPrixMoyen, prixMoyen);
     }
     //</editor-fold>
    float montantSoirée = 0;
    //<editor-fold defaultstate="collapsed" desc="code généré">
         public static final String PROP_MONTANTSOIRÉE = "montantSoirée";
         
         public float getMontantSoirée() {
             return montantSoirée;
         }
         
         public void setMontantSoirée(float montantSoirée) {
             float oldMontantSoirée = this.montantSoirée;
             this.montantSoirée = montantSoirée;
             propertyChangeSupport.firePropertyChange(PROP_MONTANTSOIRÉE, oldMontantSoirée, montantSoirée);
         }
         //</editor-fold>

         public void calculer(){
             
            float total = 7f * nbAdultes +5.5f * nbEtudiant + 4f * nbEnfant;
            
            if ( promotion ) { total=total*0.8f;}
            
            int nbPlaces = nbEtudiant+nbEnfant+nbAdultes;
            
            float prixMoyenP;
            
            if(nbPlaces>0) {prixMoyenP=total/nbPlaces;}else{prixMoyenP=0f;}
            
            setMontantSoirée(total);
            setPrixMoyen(prixMoyenP);
            
         }
        
        
}
